import axios from "axios";
import { saveAuthToken } from "../utils";

export const ApiCall = axios.create({
  baseURL: "http://localhost:8081/",
  headers: {
    post: {
      "Content-Type": "application/json",
    },
  },
});

ApiCall.interceptors.response.use(
  (response) => {
    return response.data;
  },
  (error) => {
    return Promise.reject(error.response.data?.message);
  }
);

export const configureAuthorization = (token: string, saveToken?: boolean) => {
  ApiCall.defaults.headers.common.Authorization = token;
  if (saveToken) saveAuthToken(token);
};

export const clearAuthorization = () => {
  ApiCall.defaults.headers.common.Authorization = undefined;
};

export default ApiCall;
