import { createContext, useContext } from "react";
import { IUser } from "../business/auth/api";

export interface IAppLayoutContext {
  user?: IUser;
  setUser: (user: IUser) => void;
}

export const AppLayoutContext = createContext<IAppLayoutContext>({
  user: undefined,
  setUser(user) {},
});

export const useAppLayoutContext = () => useContext(AppLayoutContext);
