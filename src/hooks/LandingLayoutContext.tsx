import { ChakraProps } from "@chakra-ui/react";
import { createContext, useContext } from "react";

export interface ILandingContext {
  url: string;
  setBackground: (img: string) => void;
  setBgStyles: (styles: ChakraProps) => void;
  reset: () => void;
}

export const LandingContext = createContext<ILandingContext>({
  url: "",
  setBackground(img) {},
  reset() {},
  setBgStyles() {},
});

export const useLandingContext = () => useContext(LandingContext);
