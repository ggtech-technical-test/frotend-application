import { Button, Icon, Stack } from "@chakra-ui/react";
import { FiLogIn } from "react-icons/fi";
import { Text } from "../../components";
import { useNavigate } from "react-router-dom";

function LogInMenuItem() {
  const navigate = useNavigate();

  const redirect = () => {
    navigate("/login");
  };

  return (
    <Button bg="transparent" colorScheme="red" onClick={redirect}>
      <Stack direction="row" align="center" spacing={2}>
        <Icon as={FiLogIn} />
        <Text>Log In</Text>
      </Stack>
    </Button>
  );
}

export default LogInMenuItem;
