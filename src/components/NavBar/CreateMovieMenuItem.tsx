import { Button, Icon, Stack } from "@chakra-ui/react";
import { FiPlusCircle } from "react-icons/fi";
import { Text } from "../../components";
import { useNavigate } from "react-router-dom";

function CreateMovieMenuItem() {
  const navigate = useNavigate();

  const clearAndRestart = () => {
    navigate("/create");
  };

  return (
    <Button bg="transparent" colorScheme="red" onClick={clearAndRestart}>
      <Stack direction="row" align="center" spacing={2}>
        <Icon as={FiPlusCircle} />
        <Text>Create</Text>
      </Stack>
    </Button>
  );
}

export default CreateMovieMenuItem;
