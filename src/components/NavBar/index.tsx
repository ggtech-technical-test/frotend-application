import { Flex, Spinner, Stack } from "@chakra-ui/react";
import Logo from "../Logo";
import Text from "../Text";
import { useCategoryList } from "../../business/category/api";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { useAppLayoutContext } from "../../hooks/AppLayoutContext";
import UserInformation from "./UserInformation";
import LogInMenuItem from "./LogInMenuItem";

export interface ICategory {
  _id: string;
  title: string;
  createdAt: Date;
  updatedAt: Date;
}

export function NavBar() {
  const { loading, request } = useCategoryList();
  const [categories, setCategories] = useState<ICategory[]>([]);
  const { user } = useAppLayoutContext();

  useEffect(() => {
    request().then((data) => {
      // @ts-expect-error
      setCategories(data?.elements);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Flex align="center" pt={5} width="full" justifyContent="space-between">
      <Stack align="center" direction="row" spacing={100}>
        <Logo
          withLink
          color="white"
          styles={{
            maxWidth: 150,
          }}
        />
        <Stack align="center" direction="row" spacing={10}>
          {loading && <Spinner color="red" size="xs" />}
          {categories.length > 0 && (
            <>
              {categories.map((category) => (
                <Link to={`/category/${category.title}`} key={category._id}>
                  <Text>{category.title}</Text>
                </Link>
              ))}
            </>
          )}
        </Stack>
      </Stack>
      <Stack align="center" direction="row" spacing={10}>
        {user ? <UserInformation /> : <LogInMenuItem />}
      </Stack>
    </Flex>
  );
}

export default NavBar;
