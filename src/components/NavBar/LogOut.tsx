import { Button, Icon, Stack } from "@chakra-ui/react";
import { FiLogOut } from "react-icons/fi";
import { Text } from "../../components";

function LogOutMenuItem() {
  const clearAndRestart = () => {
    localStorage.clear();
    // eslint-disable-next-line no-restricted-globals
    location.reload();
  };

  return (
    <Button bg="transparent" colorScheme="red" onClick={clearAndRestart}>
      <Stack direction="row" align="center" spacing={2}>
        <Icon as={FiLogOut} />
        <Text>Log Out</Text>
      </Stack>
    </Button>
  );
}

export default LogOutMenuItem;
