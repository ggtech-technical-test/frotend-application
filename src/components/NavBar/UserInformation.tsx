import { Icon, Stack } from "@chakra-ui/react";
import Text from "../Text";
import { useAppLayoutContext } from "../../hooks/AppLayoutContext";
import LogOutMenuItem from "./LogOut";
import { TbUserCircle } from "react-icons/tb";
import CreateMovieMenuItem from "./CreateMovieMenuItem";

function UserInformation() {
  const { user } = useAppLayoutContext();

  return (
    <Stack direction="row" align="center" spacing={10}>
      <Stack direction="row" align="center" spacing={2}>
        <Icon as={TbUserCircle} color="var(--red)" fontSize="1.5em" />
        <Text color="var(--red)">{user?.name}</Text>
      </Stack>
      <CreateMovieMenuItem />
      <LogOutMenuItem />
    </Stack>
  );
}

export default UserInformation;
