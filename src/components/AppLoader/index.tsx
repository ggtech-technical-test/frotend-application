import { Spinner, Stack } from "@chakra-ui/react";

export function AppLoader() {
  return (
    <Stack
      width="full"
      height="full"
      alignItems="center"
      justifyContent="center"
    >
      <Spinner color="red" size="xl" />
    </Stack>
  );
}

export default AppLoader;
