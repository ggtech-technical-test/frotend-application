import css from "./styles.module.css";
import {
  CSSProperties,
  PropsWithChildren,
  SyntheticEvent,
  useMemo,
} from "react";

export type ButtonVariantsT = "outlined" | "filled";

const stylesPresets: Record<ButtonVariantsT, CSSProperties> = {
  outlined: {
    background: "transparent",
  },
  filled: {
    width: "100%",
  },
};

export function Button({
  children,
  variant,
  styles,
  onClick,
}: PropsWithChildren<{
  variant?: ButtonVariantsT;
  styles?: CSSProperties;
  onClick?: (event: SyntheticEvent) => void;
}>) {
  const variantPreset = useMemo(
    () => stylesPresets[variant || "filled"],
    [variant]
  );

  return (
    <div
      onClick={onClick}
      style={{ ...variantPreset, ...(styles || {}) }}
      className={css.base}
    >
      {children}
    </div>
  );
}

export default Button;
