import css from "./styles.module.css";
import logo from "../../assets/logos/netflix-logo.png";
import logoWhite from "../../assets/logos/netflix-logo-white.png";
import { CSSProperties, useMemo } from "react";
import { Link } from "react-router-dom";

export function Logo({
  styles,
  color,
  withLink,
}: {
  styles?: CSSProperties;
  color?: "red" | "white";
  withLink?: boolean;
}) {
  const logoUrl = useMemo(() => {
    if (color === "white") {
      return logoWhite;
    }
    return logo;
  }, [color]);

  if (withLink) {
    return (
      <Link to="/">
        <div className={css.logoContainer} style={styles || {}}>
          <img src={logoUrl} alt="netflix" />
        </div>
      </Link>
    );
  }
  return (
    <div className={css.logoContainer} style={styles || {}}>
      <img src={logoUrl} alt="netflix" />
    </div>
  );
}

export default Logo;
