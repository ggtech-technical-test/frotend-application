import { CSSProperties, PropsWithChildren, useMemo } from "react";

export type TextPresetsT = "p" | "h1" | "h2" | "h3";

const stylesPresets: Record<TextPresetsT, CSSProperties> = {
  p: {
    fontSize: "1em",
    fontWeight: "400",
    lineHeight: "1.2",
    fontFamily: "Roboto",
  },
  h1: {
    fontSize: "2em",
    fontWeight: "600",
    lineHeight: "1.5",
    textAlign: "center",
    fontFamily: "Roboto",
  },
  h2: {
    fontSize: "1.8em",
    fontWeight: "600",
    lineHeight: "1.5",
    textAlign: "center",
    fontFamily: "Roboto",
  },
  h3: {
    fontSize: "1.5em",
    fontWeight: "400",
    lineHeight: "1.5",
    fontFamily: "Roboto",
  },
};

export function Text({
  variant,
  color,
  styles,
  children,
}: PropsWithChildren<{
  variant?: TextPresetsT;
  color?: string;
  styles?: CSSProperties;
}>) {
  const textPresetStyles = useMemo(
    () => stylesPresets[variant || "p"],
    [variant]
  );

  return (
    <p
      style={{
        ...textPresetStyles,
        color: color || "auto",
        ...styles,
      }}
    >
      {children}
    </p>
  );
}

export default Text;
