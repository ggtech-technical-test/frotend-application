import { Suspense, lazy } from "react";
import {
  Route,
  RouterProvider,
  createBrowserRouter,
  createRoutesFromElements,
} from "react-router-dom";
import { AppLoader } from "../components";
import AppLayout from "../layouts/App";
import AuthorizationLayout from "../layouts/Authorization";
import LandingPageLayout from "../layouts/Landing";
const HomePage = lazy(() => import("../views/HomePage"));
const CreateMovie = lazy(() => import("../views/CreateMovie"));
const LoginPage = lazy(() => import("../views/Login"));
const Registration = lazy(() => import("../views/Registration"));

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route path="/" element={<AppLayout />}>
      <Route element={<LandingPageLayout />}>
        <Route
          index
          element={
            <Suspense fallback={<AppLoader />}>
              <HomePage />
            </Suspense>
          }
        />
        <Route
          path="create"
          element={
            <Suspense fallback={<AppLoader />}>
              <CreateMovie />
            </Suspense>
          }
        />
      </Route>
      <Route element={<AuthorizationLayout />}>
        <Route
          path="login"
          element={
            <Suspense fallback={<AppLoader />}>
              <LoginPage />
            </Suspense>
          }
        />
        <Route
          path="register"
          element={
            <Suspense fallback={<AppLoader />}>
              <Registration />
            </Suspense>
          }
        />
      </Route>
    </Route>
  )
);

function AppRouting() {
  return <RouterProvider router={router} />;
}

export default AppRouting;
