import { useState } from "react";
import { regularResponseT } from "../../auth/api";
import ApiCall from "../../../api";

export interface IPlatform {
  _id: string;
  title: string;
  createdAt: Date;
  updatedAt: Date;
  icon: string;
}

export const usePlatformsList = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string | undefined>("");

  const send = async (): Promise<regularResponseT> => {
    try {
      setLoading(true);
      setError(undefined);
      const response = await ApiCall.get<regularResponseT>("platform");
      return response.data;
    } catch (error) {
      setError(error as string);
      return {
        message: error as string,
        error: true,
      };
    } finally {
      setLoading(false);
    }
  };

  return { loading, request: send, error };
};
