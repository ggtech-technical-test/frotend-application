import {
  Button,
  FormControl,
  FormLabel,
  Spinner,
  Stack,
} from "@chakra-ui/react";
import { IPlatform, usePlatformsList } from "../../api";
import { useEffect, useState } from "react";

export function PlatformsSelector({
  onChange,
}: {
  onChange: (ids: string[]) => void;
}) {
  const [platforms, setPlatforms] = useState<IPlatform[]>([]);
  const { loading, request } = usePlatformsList();
  const [selectedIds, selectPlatform] = useState<Set<string>>(new Set([]));

  const select = (_id: string) => {
    const nextIds = new Set(selectedIds);
    if (selectedIds.has(_id)) {
      nextIds.delete(_id);
    } else {
      nextIds.add(_id);
    }
    selectPlatform(nextIds);
  };

  useEffect(() => {
    onChange(Array.from(selectedIds));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedIds]);

  useEffect(() => {
    request().then((response) => {
      // @ts-expect-error
      if (!request?.error) {
        // @ts-expect-error
        setPlatforms(response.elements);
      }
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <FormControl>
      <FormLabel>Platforms</FormLabel>
      <Stack direction="row" wrap="wrap" alignItems="center" spacing={5}>
        {loading && <Spinner color="red" size="xs" />}
        {platforms?.length > 0 && (
          <>
            {platforms.map((platform) => (
              <Button
                onClick={() => select(platform._id)}
                colorScheme={
                  selectedIds.has(platform._id) ? "red" : "whiteAlpha"
                }
                key={platform._id}
              >
                {platform.title}
              </Button>
            ))}
          </>
        )}
      </Stack>
    </FormControl>
  );
}

export default PlatformsSelector;
