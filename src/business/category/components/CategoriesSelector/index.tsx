import { FormControl, FormLabel, Select, Spinner } from "@chakra-ui/react";
import { useCategoryList } from "../../api";
import { useEffect, useState } from "react";
import { ICategory } from "../../../../components";

function CategoriesSelector({
  onChange,
}: {
  onChange: (ids: string[]) => void;
}) {
  const [categories, setCategories] = useState<ICategory[]>([]);
  const { loading, request } = useCategoryList();

  useEffect(() => {
    request().then((resp) => {
      if (!resp.error) {
        // @ts-expect-error
        setCategories(resp.elements);
      }
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <FormControl>
      <FormLabel>Categories</FormLabel>
      {loading && <Spinner color="red" size="xs" />}
      {categories.length > 0 && (
        <Select
          placeContent="Select Movie Categories"
          onChange={(event) => {
            onChange([event.target.value]);
          }}
        >
          {categories.map((category) => (
            <option key={category._id} value={category._id}>
              {category.title}
            </option>
          ))}
        </Select>
      )}
    </FormControl>
  );
}

export default CategoriesSelector;
