import { useState } from "react";
import ApiCall, { configureAuthorization } from "../../../api";
import { LoginFormT } from "../components/LogInForm";
import { saveUserData } from "../../../utils";

export interface IUser {
  _id?: string;
  role: number;
  name: string;
  lastName: string;
  password: string;
  email: string;
  createdAt: Date;
  updatedAt: Date;
}

export type loginResponseT = {
  data?: { token: string; user: IUser };
  message?: string;
  error?: boolean;
};

export const useLoginRequest = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string | undefined>("");

  const send = async (data: LoginFormT): Promise<loginResponseT> => {
    try {
      setLoading(true);
      setError(undefined);
      const response = await ApiCall.post("login", { ...data });
      configureAuthorization(response.data.token, true);
      saveUserData(response.data.user);
      return response.data;
    } catch (error) {
      setError(error as string);
      return {
        message: error as string,
        error: true,
      };
    } finally {
      setLoading(false);
    }
  };

  return { loading, request: send, error };
};

export type regularResponseT = {
  data?: {
    elements?: object[];
    count?: number;
  };
  message?: string;
  error?: boolean;
};

export const useRegisterRequest = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string | undefined>("");

  const send = async (data: LoginFormT): Promise<regularResponseT> => {
    try {
      setLoading(true);
      setError(undefined);
      const response = await ApiCall.post<regularResponseT>("user/create", {
        ...data,
      });
      // @ts-expect-error
      return response.message;
    } catch (error) {
      setError(error as string);
      return {
        message: error as string,
        error: true,
      };
    } finally {
      setLoading(false);
    }
  };

  return { loading, request: send, error };
};
