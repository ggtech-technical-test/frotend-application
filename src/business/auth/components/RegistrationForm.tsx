import { useForm } from "react-hook-form";
import { Button, Text } from "../../../components";
import {
  FormControl,
  FormHelperText,
  FormLabel,
  Input,
  Spinner,
  Stack,
} from "@chakra-ui/react";
import { useRegisterRequest } from "../api";
import { Link, useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";

export type RegistrationFormT = {
  email: string;
  name: string;
  lastName: string;
  password: string;
  password_confirmation: string;
};

function RegistrationForm() {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<RegistrationFormT>({
    defaultValues: {
      email: "",
      name: "",
      lastName: "",
      password: "",
      password_confirmation: "",
    },
  });

  const navigate = useNavigate();
  const { loading, request, error } = useRegisterRequest();
  const [redirect, setRedirect] = useState<string>("");

  useEffect(() => {
    if (redirect) {
      const timeout = setTimeout(() => {
        navigate("/login");
      }, 5000);
      return () => {
        clearTimeout(timeout);
      };
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [redirect]);

  const onSubmit = (values: RegistrationFormT) => {
    request(values).then((message) => {
      setRedirect(message as string);
    });
  };

  return (
    <Stack spacing={10} width="full">
      <Text variant="h2">Create a user</Text>
      <Stack spacing={3} direction="column" align="center" width="100%">
        <FormControl>
          <FormLabel>Email</FormLabel>
          <Input type="email" {...register("email", { required: true })} />
          {"email" in errors && (
            <FormHelperText color="yellow.500">
              {`${errors.email?.type} - ${errors.email?.message}`}
            </FormHelperText>
          )}
        </FormControl>
        <FormControl>
          <FormLabel>Name</FormLabel>
          <Input type="text" {...register("name", { required: true })} />
          {"name" in errors && (
            <FormHelperText color="yellow.500">
              {`${errors.name?.type} - ${errors.name?.message}`}
            </FormHelperText>
          )}
        </FormControl>
        <FormControl>
          <FormLabel>Last Name</FormLabel>
          <Input type="text" {...register("lastName", { required: true })} />
          {"lastName" in errors && (
            <FormHelperText color="yellow.500">
              {`${errors.lastName?.type} - ${errors.lastName?.message}`}
            </FormHelperText>
          )}
        </FormControl>
        <FormControl>
          <FormLabel>Password</FormLabel>
          <Input
            type="password"
            {...register("password", { required: true })}
          />
          {"password" in errors && (
            <FormHelperText color="yellow.500">
              {`${errors.password?.type} - ${errors.password?.message}`}
            </FormHelperText>
          )}
        </FormControl>
        <FormControl>
          <FormLabel>Password Confirmation</FormLabel>
          <Input
            type="password"
            {...register("password_confirmation", {
              required: true,
              deps: "password",
              validate: (value, formValues) => {
                return value === formValues.password;
              },
            })}
          />
          {"password_confirmation" in errors && (
            <FormHelperText color="yellow.500">
              {`${errors.password_confirmation?.type} - ${errors.password_confirmation?.message}`}
            </FormHelperText>
          )}
        </FormControl>
        {error && <Text color="var(--red)">{error}</Text>}
      </Stack>
      <Stack spacing={5} width="full" alignItems="center">
        {loading && <Spinner color="red" size="sm" />}
        {redirect && <Text color="var(--white)">{redirect}</Text>}
        <Button onClick={handleSubmit(onSubmit)}>Register your account</Button>
        <Link to="/login">
          <Text color="var(--red-light)">
            already have an account? Login here
          </Text>
        </Link>
      </Stack>
    </Stack>
  );
}

export default RegistrationForm;
