import { useForm } from "react-hook-form";
import { Button, Text } from "../../../components";
import {
  FormControl,
  FormHelperText,
  FormLabel,
  Input,
  Spinner,
  Stack,
} from "@chakra-ui/react";
import { useLoginRequest } from "../api";
import { Link, useNavigate } from "react-router-dom";
import { useAppLayoutContext } from "../../../hooks/AppLayoutContext";

export type LoginFormT = {
  email: string;
  password: string;
};

function LoginForm() {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<LoginFormT>({
    defaultValues: {
      email: "",
      password: "",
    },
  });

  const navigate = useNavigate();
  const { loading, request, error } = useLoginRequest();
  const { setUser } = useAppLayoutContext();

  const onSubmit = (values: LoginFormT) => {
    request(values).then((response) => {
      if (!response.error) {
        // @ts-expect-error
        setUser(response.user);
        navigate("/");
      }
    });
  };

  return (
    <Stack spacing={10} width="full">
      <Text variant="h2">Login</Text>
      <Stack spacing={5} direction="column" align="center" width="100%">
        <FormControl>
          <FormLabel>Email</FormLabel>
          <Input type="email" {...register("email", { required: true })} />
          {"email" in errors && (
            <FormHelperText color="yellow.500">
              {`${errors.email?.type} - ${errors.email?.message}`}
            </FormHelperText>
          )}
        </FormControl>
        <FormControl>
          <FormLabel>Password</FormLabel>
          <Input
            type="password"
            {...register("password", { required: true })}
          />
          {"password" in errors && (
            <FormHelperText color="yellow.500">
              {`${errors.password?.type} - ${errors.password?.message}`}
            </FormHelperText>
          )}
        </FormControl>
        {error && <Text color="var(--red)">{error}</Text>}
        {loading && <Spinner color="red" size="sm" />}
      </Stack>
      <Stack spacing={3} width="full" alignItems="center">
        <Button onClick={handleSubmit(onSubmit)}>Login your account</Button>
        <Link to="/register">
          <Text color="var(--red-light)">
            you don't have an account? register here
          </Text>
        </Link>
      </Stack>
    </Stack>
  );
}

export default LoginForm;
