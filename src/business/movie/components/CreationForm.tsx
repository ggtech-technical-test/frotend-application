import { Controller, useForm } from "react-hook-form";
import { Button, PlatformsSelector, Text } from "../../../components";
import {
  FormControl,
  FormHelperText,
  FormLabel,
  Input,
  Spinner,
  Stack,
} from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";
import { useMovieCreation } from "../api";
import CategoriesSelector from "../../category/components/CategoriesSelector";

export type MovieCreationFormT = {
  title: string;
  image: string;
  director: string;
  description: string;
  platforms: string[];
  categories: string[];
};

function MoveCreationForm() {
  const {
    register,
    handleSubmit,
    formState: { errors },
    control,
  } = useForm<MovieCreationFormT>({
    defaultValues: {
      title: "",
      image: "",
      director: "",
      description: "",
      platforms: [],
      categories: [],
    },
  });

  const navigate = useNavigate();
  const { loading, request, error } = useMovieCreation();

  const onSubmit = (values: MovieCreationFormT) => {
    request(values).then((response) => {
      if (!response.error) {
        navigate("/");
      }
    });
  };

  return (
    <Stack spacing={10} width="full">
      <Text variant="h2">Create a movie to rate</Text>
      <Stack spacing={5} direction="column" align="center" width="100%">
        <FormControl>
          <FormLabel>Movie Title</FormLabel>
          <Input type="text" {...register("title", { required: true })} />
          {"title" in errors && (
            <FormHelperText color="yellow.500">
              {`${errors.title?.type} - ${errors.title?.message}`}
            </FormHelperText>
          )}
        </FormControl>
        <FormControl>
          <FormLabel>Image url</FormLabel>
          <Input type="text" {...register("image", { required: true })} />
          {"image" in errors && (
            <FormHelperText color="yellow.500">
              {`${errors.image?.type} - ${errors.image?.message}`}
            </FormHelperText>
          )}
        </FormControl>
        <FormControl>
          <FormLabel>Directed By:</FormLabel>
          <Input type="text" {...register("director", { required: true })} />
          {"director" in errors && (
            <FormHelperText color="yellow.500">
              {`${errors.director?.type} - ${errors.director?.message}`}
            </FormHelperText>
          )}
        </FormControl>
        <FormControl>
          <FormLabel>Description:</FormLabel>
          <Input type="text" {...register("description", { required: true })} />
          {"description" in errors && (
            <FormHelperText color="yellow.500">
              {`${errors.description?.type} - ${errors.description?.message}`}
            </FormHelperText>
          )}
        </FormControl>
        <Controller
          name="platforms"
          control={control}
          rules={{
            required: true,
          }}
          render={({ field: { onChange } }) => (
            <PlatformsSelector onChange={(ids) => onChange(ids)} />
          )}
        />
        <Controller
          name="categories"
          control={control}
          rules={{
            required: true,
          }}
          render={({ field: { onChange } }) => (
            <CategoriesSelector onChange={(ids) => onChange(ids)} />
          )}
        />
      </Stack>
      {error && <Text color="var(--red)">{error}</Text>}
      {loading && <Spinner color="red" size="sm" />}
      <Button onClick={handleSubmit(onSubmit)}>Create Movie</Button>
    </Stack>
  );
}

export default MoveCreationForm;
