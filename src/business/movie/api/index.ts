import { useState } from "react";
import { MovieCreationFormT } from "../components/CreationForm";
import ApiCall from "../../../api";

export const useMovieCreation = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string | undefined>("");

  const send = async (data: MovieCreationFormT) => {
    try {
      setLoading(true);
      setError(undefined);
      const response = await ApiCall.post("movie/create", { ...data });
      return response.data;
    } catch (error) {
      setError(error as string);
      return {
        message: error as string,
        error: true,
      };
    } finally {
      setLoading(false);
    }
  };

  return { loading, request: send, error };
};
