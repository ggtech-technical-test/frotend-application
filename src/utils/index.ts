import { IUser } from "../business/auth/api";

export const saveAuthToken = (token: string) => {
  localStorage.setItem("auth", token);
};

export const getAuthToken = (): string | null => {
  const token = localStorage.getItem("auth");
  return token;
};

export const saveUserData = (user: IUser) => {
  localStorage.setItem("userInf", JSON.stringify(user));
};

export const getUserData = (): IUser | null => {
  const user = localStorage.getItem("userInf");
  if (user) {
    return JSON.parse(user);
  }
  return null;
};
