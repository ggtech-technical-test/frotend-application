import { ChakraProvider, extendTheme } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { Outlet } from "react-router-dom";
import { getAuthToken, getUserData } from "../utils";
import { configureAuthorization } from "../api";
import { AppLayoutContext } from "../hooks/AppLayoutContext";
import { IUser } from "../business/auth/api";

const theme = extendTheme({
  styles: {
    global: {
      "html, body": {
        backgroundColor: "black",
        color: "white",
      },
    },
  },
  fonts: {
    body: "Roboto",
    heading: "Roboto",
    mono: "Roboto",
  },
});

function AppLayout() {
  const [user, setUser] = useState<IUser>();

  useEffect(() => {
    const token = getAuthToken();
    if (token) {
      configureAuthorization(token);
    }
  }, []);

  useEffect(() => {
    const user = getUserData();
    if (user) {
      setUser(user);
    }
  }, []);

  return (
    <ChakraProvider theme={theme}>
      <AppLayoutContext.Provider
        value={{
          user: user,
          setUser: (nextUser) => setUser(nextUser),
        }}
      >
        <Outlet />
      </AppLayoutContext.Provider>
    </ChakraProvider>
  );
}

export default AppLayout;
