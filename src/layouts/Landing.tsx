import { Box, ChakraProps, Flex } from "@chakra-ui/react";
import { Outlet } from "react-router-dom";
import { NavBar } from "../components";
import { LandingContext } from "../hooks/LandingLayoutContext";
import { useMemo, useState } from "react";

function LandingPageLayout() {
  const [backgroundImage, setBackground] = useState<string>("");
  const [bgStyles, setBgStyles] = useState<ChakraProps>({});

  const backgroundStyles = useMemo((): ChakraProps => {
    if (backgroundImage) {
      return {
        bgImage: `url("${backgroundImage}")`,
        ...bgStyles,
      };
    }
    return {};
  }, [backgroundImage, bgStyles]);

  return (
    <Flex
      w="full"
      direction="column"
      alignItems="center"
      justifyContent="flex-start"
      {...backgroundStyles}
    >
      <Flex
        minH="100vh"
        w="full"
        maxW="var(--content-width)"
        justifyContent="flex-start"
        direction="column"
      >
        <LandingContext.Provider
          value={{
            url: backgroundImage,
            setBackground: (image: string) => setBackground(image),
            reset: () => setBackground(""),
            setBgStyles: (styles) => setBgStyles(styles),
          }}
        >
          <NavBar />
          <Box pt={10}>
            <Outlet />
          </Box>
        </LandingContext.Provider>
      </Flex>
    </Flex>
  );
}

export default LandingPageLayout;
