import { Outlet, useLocation } from "react-router-dom";
import css from "./styles.module.css";
import { useMemo } from "react";
import loginBg from "../../assets/login-background.png";
import signUp from "../../assets/signup-background.png";

function AuthorizationLayout() {
  const route = useLocation();

  const backgroundPicture = useMemo(() => {
    if (route.pathname === "/login") {
      return loginBg;
    }
    return signUp;
  }, [route.pathname]);

  return (
    <div
      className={css.wrapper}
      style={{
        backgroundImage: `url("${backgroundPicture}")`,
      }}
    >
      <div className={css.container}>
        <Outlet />
      </div>
    </div>
  );
}

export default AuthorizationLayout;
