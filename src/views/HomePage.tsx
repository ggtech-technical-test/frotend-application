import { Text } from "../components";

function HomePage() {
  return (
    <div className="App">
      <Text variant="h1">Home Page</Text>
    </div>
  );
}

export default HomePage;
