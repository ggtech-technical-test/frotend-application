import { useEffect } from "react";
import { useLandingContext } from "../../hooks/LandingLayoutContext";
import background from "../../assets/creation-background.png";
import { Flex } from "@chakra-ui/react";
import MoveCreationForm from "../../business/movie/components/CreationForm";

function CreateMovie() {
  const { setBackground, setBgStyles, reset } = useLandingContext();
  useEffect(() => {
    setBackground(background);
    setBgStyles({
      bgRepeat: "no-repeat",
      bgSize: "cover",
    });
    return () => reset();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Flex
      w="full"
      direction="column"
      justifyContent="flex-start"
      align="center"
    >
      <Flex
        w="45%"
        p={8}
        borderRadius="var(--border-radius)"
        bg="var(--gradient-darker)"
        direction="column"
        align="center"
      >
        <MoveCreationForm />
      </Flex>
    </Flex>
  );
}

export default CreateMovie;
