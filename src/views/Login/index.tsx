import { Box, Flex } from "@chakra-ui/react";
import { Logo, Text } from "../../components";
import LoginForm from "../../business/auth/components/LogInForm";

function Login() {
  return (
    <Flex w="full" h="full" justifyContent="space-between">
      <Flex w="50%" h="full" direction="column" justify="center" align="center">
        <Flex
          w="70%"
          p={8}
          borderRadius="var(--border-radius)"
          bg="var(--gradient-black)"
          direction="column"
          align="center"
        >
          <LoginForm />
        </Flex>
      </Flex>
      <Flex w="50%" h="full" direction="column" justify="center" align="center">
        <Box>
          <Logo
            styles={{
              width: 300,
            }}
          />
          <Text
            variant="h3"
            styles={{
              textAlign: "center",
              paddingTop: 20,
            }}
          >
            Movies and Experiences
          </Text>
        </Box>
      </Flex>
    </Flex>
  );
}

export default Login;
